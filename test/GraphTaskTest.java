import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void testfindFurthestLocatedVertecisFromGivenVertex() {

      GraphTask.Vertex v = new GraphTask.Vertex("v1");
      GraphTask.Graph g = new GraphTask.Graph("MainGraph", v);

      ArrayList<GraphTask.Vertex> vArray = g.findFurthestLocatedVertecisFromGivenVertex(v);

      assertTrue("Returns only one vertex when graph contains one vertex", vArray.size() == 1);

      assertTrue ("Returns correct vertex when graph contains one vertex", v == vArray.get(0));

      assertTrue("The level of Vertex should be -1 after method is used", vArray.get(0).getLevel() == -1);

      GraphTask.Arc a = new GraphTask.Arc("v2_v1",v,null);
      GraphTask.Vertex v2 = new GraphTask.Vertex("v2",v,a);

      vArray = g.findFurthestLocatedVertecisFromGivenVertex(v2);

      assertTrue("Should return an empty array if the graph doesn't contain the vertex", vArray.size() == 0);

      vArray = g.findFurthestLocatedVertecisFromGivenVertex(null);
      assertTrue("Should return an empty array if the input is null", vArray.size() == 0);

      g = new GraphTask.Graph("MainGraph");
      vArray = g.findFurthestLocatedVertecisFromGivenVertex(v);
      assertTrue("Should return an empty array if the graph doesn't contain vertices", vArray.size() == 0);

   }

}

