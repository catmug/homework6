import java.util.*;
public class GraphTask {

   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (100, 200);

      List<Vertex> vertexList = g.findFurthestLocatedVertecisFromGivenVertex(g.first.next.next.next);

      for (Vertex v :
              vertexList) {
         System.out.println("The furthest was " + v.id);
      }


      System.out.println (g);
   }
   /**
    * helper method to display a 2dimensional array in string format
    * @param a array that you wish to view as a string
    * @return String of the array in the format of |%3d| and with linebreaks
    */
   public String matrixToString(int[][] a){
      StringBuilder sb = new StringBuilder();
      for (int[] i:a) {
         for (int j: i) {
            sb.append(String.format("|%3d| ",j));
         }
         sb.append("\n");
      }
      return sb.toString();
   }

   static class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private int level = -1;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      /**
       * @return int vertex info
       */
      public int getInfo() {
         return info;
      }

      /**
       * set the value of info field
       * @param info info to be set in integer format
       */
      public void setInfo(int info) {
         this.info = info;
      }

      /**
       * returns the level of the vertex
       */
      public int getLevel() {return level; }

      /**
       * @return boolean weather the vertex has a next vertex
       */
      public boolean hasNext(){
         return next != null;
      }


      /**
       * helper method: returns all arcs that are going out of the current instance of the vertex
       * @return ArrayList<Arc> arcs that are going out of the current instance of the vertex
       */
      public ArrayList<Arc> getOutgoingArcs(){
         ArrayList<Arc> arcs = new ArrayList<Arc>();

         if(first == null){
            return arcs;
         }

         Arc arc = first;
         do{
            arcs.add(arc);
            arc = arc.next;
         }while(arc.hasNext());
         arcs.add(arc);

         return arcs;
      }
   }


   static class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      /**
       * @return boolean whether arc has a next arc
       */
      public boolean hasNext(){
         return next != null;
      }

   }


   static class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      /*
      * Searches for a vertex in the graph
      * @param v vertex to be searched
      * @return boolean whether the graph contains the vertex or not
      */
      public boolean containsVertex(Vertex v){

         if(first == null){
            return false;
         }

         Vertex vert = first;
         do{
            if(vert.equals(v)) {return true;}
            if(!vert.hasNext()){break;}
            vert = vert.next;
         }while(vert.hasNext());
         if(vert.equals(v)) {return true;}

         return false;
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Traverse the graph breadth-first and give each vertex a level number how far is it from the input vertex.
       * Method is based upon Jaanus Pöials Breadth-first traversal of a graph "Läbimine laiuti"
       * http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
       * @param s source vertex
       * @return ArrayList<Vertex> of vertices furthest away from source vertex
       */
      public ArrayList<Vertex> findFurthestLocatedVertecisFromGivenVertex (Vertex s) {
         if(!this.containsVertex(s))
            return new ArrayList<Vertex>();

         Queue vq = new Queue();

         int level = 1;
         s.level = 0;
         vq.enqueue(s);

         while (vq.getSize() > 0) {

            Vertex v = vq.dequeue();

            if(v.first != null){
               boolean isLast = true;
               Arc arc = v.first;
               while(true){
                  if(arc.target.level == -1) {
                     arc.target.level = level;
                     vq.enqueue(arc.target);
                  }
                  isLast = !arc.hasNext();
                  if(isLast){break;}
                  else{arc = arc.next;}
               }
               level++;
            }
         }

         ArrayList<Vertex> vertexList = new ArrayList<Vertex>();
         Vertex v = this.first;
         level = 0;
         while(v.hasNext()){
            if(v.level > level){
               level = v.level;
            }
            v = v.next;
         }
         if(v.level > level){
            level = v.level;
         }

         v = this.first;
         while(v.hasNext()){
            if(v.level == level){
               vertexList.add(v);
               v.level = -1;
            }
            v = v.next;
         }
         if(v.level == level){
            vertexList.add(v);
            v.level = -1;
         }

         return vertexList;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }
   }

   public static class Queue {
      private ArrayList<Vertex> a;
      private int size = 0;
      Queue(){
         a = new ArrayList<Vertex>();
      }

      /**
       * Gives the current size of the queue
       */
      public int getSize() {
         return size;
      }

      /**
       * Increments queue size by one
       */
      private void increaseSize(){
         size++;
      }

      /**
       * decrements queue size by one
       */
      private void decreaseSize(){
         if(size == 0)
            throw new RuntimeException("Queue size can not be lower than 0.");
         size--;
      }

      /**
       * add a vertex to the gueue
       * @param e vertex to be added to the queue
       */
      public void enqueue(Vertex e){
         a.add(e);
         increaseSize();
      }

      /**
       * remove and return a vertex from the gueue
       * @return Vertex that was removed from the queue
       */
      public Vertex dequeue(){
         decreaseSize();
         return a.remove(0);
      }
   }

}

